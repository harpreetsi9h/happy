package com.example.android.happy.HelperClass

import android.animation.Animator
import android.content.Context
import android.view.View
import android.view.animation.Animation

/**
 * Created by developtech on 1/8/18.
 */
class MyAnimationListener(view : View, isOpen : Boolean) : Animator.AnimatorListener {


    var view : View
    var isOpen : Boolean

    init {
        this.view = view
        this.isOpen = isOpen
    }


    override fun onAnimationRepeat(p0: Animator?) {

    }

    override fun onAnimationEnd(p0: Animator?) {
        if(isOpen)
        {
            view.visibility = View.GONE
        }
    }

    override fun onAnimationCancel(p0: Animator?) {

    }

    override fun onAnimationStart(p0: Animator?) {
        if(!isOpen)
        {
            view.visibility = View.VISIBLE
        }

    }

}