package com.example.android.happy.Fragments


import android.annotation.SuppressLint
import android.os.Bundle
import android.support.v4.app.Fragment
import android.text.Editable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import butterknife.BindView
import butterknife.ButterKnife
import butterknife.OnClick
import com.example.android.happy.Activity.FragmentsDemoActivity

import com.example.android.happy.R

class OneFragment : Fragment() {

    @BindView(R.id.etName)
    lateinit var etName : EditText

    @BindView(R.id.etPhone)
    lateinit var etPhone : EditText

    var activity : FragmentsDemoActivity? = null

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        activity = getActivity() as FragmentsDemoActivity
        val view : View = inflater!!.inflate(R.layout.fragment_one, container, false)
        ButterKnife.bind(this, view)
        return view
    }

    @SuppressLint("NewApi")
    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        activity!!.toolbar.title = "One"
        var bundle : Bundle? = arguments
        if(bundle!=null) {
            etName.setText(bundle.getString("name"))
            etPhone.setText(bundle.getString("phone"))
        }

    }

    @OnClick(R.id.btnSubmit)
    public fun onClick(view : View)
    {
        when(view.id)
        {
            R.id.btnSubmit->
            {
                val frag : Fragment = TwoFragment()
                activity!!.supportFragmentManager.beginTransaction().replace(R.id.contentLayout, frag).commit()
            }
        }
    }
}
