package com.example.android.happy.Activity

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.CardView
import android.support.v7.widget.LinearLayoutCompat
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.widget.LinearLayout
import butterknife.BindView
import butterknife.ButterKnife
import com.aurelhubert.ahbottomnavigation.AHBottomNavigation
import com.aurelhubert.ahbottomnavigation.AHBottomNavigationItem
import com.example.android.happy.Adapter.CollapsingAdaper
import com.example.android.happy.R


class CollapsingToolbarActivity : AppCompatActivity() {

    @BindView(R.id.recyclerView)
    lateinit var recycler : RecyclerView

    @BindView(R.id.bottom_navigation)
    lateinit var bottomNavigation : AHBottomNavigation

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_collapsing)
        ButterKnife.bind(this)

        recycler.layoutManager = LinearLayoutManager(this, LinearLayout.VERTICAL, false)
        recycler.adapter = CollapsingAdaper()

        bottomNavigation = findViewById(R.id.bottom_navigation) as AHBottomNavigation

        val item1 = AHBottomNavigationItem(resources.getString(R.string.item_msg),
                resources.getDrawable(R.drawable.ic_msg_black))


        val item2 = AHBottomNavigationItem(resources.getString(R.string.item_music),
                resources.getDrawable(R.drawable.ic_music_black))

        val item3 = AHBottomNavigationItem(resources.getString(R.string.item_images),
                resources.getDrawable(R.drawable.ic_images))


        bottomNavigation!!.addItem(item1)
        bottomNavigation!!.addItem(item2)
        bottomNavigation!!.addItem(item3)

    }
}
