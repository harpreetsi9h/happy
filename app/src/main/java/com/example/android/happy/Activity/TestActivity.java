package com.example.android.happy.Activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.android.happy.R;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;
import java.util.concurrent.Callable;

import rx.Observable;
import rx.Observer;
import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class TestActivity extends AppCompatActivity {

    LinearLayout rootLayout;

    TextView txtPart1;
    Button btnBegin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test);

        rootLayout = (LinearLayout) findViewById(R.id.rootLayout);

        txtPart1 = (TextView) findViewById(R.id.txtPart1);
        btnBegin = (Button) findViewById(R.id.btnBegin);

        btnBegin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                createObservable();
            }
        });



    }


    void createObservable() {
        Subscription mTvShowSubscription;
        Observable<List<String>> tvShowObservable = Observable.fromCallable(new Callable<List<String>>() {

            @Override
            public List<String> call() {
                return getColorList();
            }
        });

        mTvShowSubscription = tvShowObservable
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<List<String>>() {

                    @Override
                    public void onCompleted() {
                    }

                    @Override
                    public void onError(Throwable e) {
                    }

                    @Override
                    public void onNext(List<String> tvShows) {
                        StringBuilder sb = new StringBuilder();
                        for (String s : tvShows) {
                            sb.append(" " + s);
                            txtPart1.setText(sb);
                        }

                    }
                });

//        Observable<List<String>> listObservable = Observable.just(getColorList());
//
//        listObservable.subscribe(new Observer<List<String>>() {
//
//            @Override
//            public void onCompleted() { }
//
//            @Override
//            public void onError(Throwable e) { }
//
//            @Override
//            public void onNext(List<String> colors) {
//                StringBuilder sb = new StringBuilder();
//                sb.append(" "+colors);
//                txtPart1.setText(sb);
//            }
//        });


//        Observable.OnSubscribe observableAction = new Observable.OnSubscribe<String>() {
//            public void call(Subscriber<? super String> subscriber) {
//                subscriber.onNext("Hello World!");
//                subscriber.onCompleted();
//            }
//        };
//        Observable<String> observable = Observable.create(observableAction);
//
//        Subscriber<String> textViewSubscriber = new Subscriber<String>() {
//            public void onCompleted() {}
//            public void onError(Throwable e) {}
//            public void onNext(String s) {
//                txtPart1.setText(s);
//            }
//        };
//
//        Subscriber<String> toastSubscriber = new Subscriber<String>() {
//            public void onCompleted() {}
//            public void onError(Throwable e) {}
//            public void onNext(String s) {
//                Toast.makeText(getApplicationContext(), s, Toast.LENGTH_SHORT).show();
//            }
//        };
//
//
//        observable.observeOn(AndroidSchedulers.mainThread());
//        observable.subscribe(textViewSubscriber);
//        observable.subscribe(toastSubscriber);

    }

    public List<String> getColorList() {
        List<String> colorList = new ArrayList<>(Arrays.asList("Black","Green","Orange","Blue","Cyan","Pink","Red","Grey","White"));
        return colorList;
    }

}
