package com.example.android.happy

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.design.widget.TextInputLayout
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.FrameLayout
import android.widget.Toast
import com.google.android.gms.tasks.Task
import com.google.firebase.auth.AuthResult
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import android.view.animation.AnimationUtils
import android.app.Activity
import android.view.inputmethod.InputMethodManager
import com.example.android.happy.Activity.CollapsingToolbarActivity


class LoginActivity : AppCompatActivity(), View.OnClickListener
{


    var btnOne : Button? = null
    var btnTwo : Button? = null
    var tilEmail : TextInputLayout? = null
    var tilPass : TextInputLayout? = null
    var etEmail : EditText? = null
    var etPass : EditText? = null
    var mAuth : FirebaseAuth? = null
    var currentUser : FirebaseUser? = null
    var user : FirebaseUser? = null
    var loader : FrameLayout? = null
    var rootLayout : FrameLayout? = null
    var perference : SharedPreferences? = null

    var login : Boolean? = true

    override fun onClick(v: View?) {

        v?.let {

            when(v.id)
            {
                R.id.btnOne ->
                {
                    if(!validate())
                        return

                    hideKeyboard(this)

                    showLoader()

                    startActivity(Intent(this, MainActivity::class.java))
//                    if(login!!) login() else signUp()

                }
                R.id.btnTwo ->
                {
                    if(login!!){
                        login = false
                    }
                    else
                    {
                        login = true
                    }

                    switch()
                }
                else -> {
                }
            }
        }
    }


    fun showLoader()
    {
        loader!!.visibility = View.VISIBLE
    }

    fun hideLoader()
    {
        loader!!.visibility = View.GONE
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        init()

        switch()


    }

    fun init()
    {
        mAuth = FirebaseAuth.getInstance()

        btnOne = findViewById(R.id.btnOne) as Button
        btnTwo = findViewById(R.id.btnTwo) as Button

        tilEmail = findViewById(R.id.tilEmail) as TextInputLayout
        tilPass = findViewById(R.id.tilPass) as TextInputLayout

        etEmail = findViewById(R.id.etEmail) as EditText
        etPass = findViewById(R.id.etPassword) as EditText

        loader = findViewById(R.id.loader) as FrameLayout
        rootLayout = findViewById(R.id.rootLayout) as FrameLayout

        btnOne!!.setOnClickListener(this)
        btnTwo!!.setOnClickListener(this)
    }


    fun switch()
    {

        val fadeInAnimation = AnimationUtils.loadAnimation(this, R.anim.fade_in)

        if(login!!){
            btnOne!!.setText("Login")
            btnTwo!!.setText("Sign Up")
            rootLayout!!.startAnimation(fadeInAnimation)
        }
        else
        {
            btnOne!!.setText("Sign Up")
            btnTwo!!.setText("Login")
            rootLayout!!.startAnimation(fadeInAnimation)
        }
    }

    fun validate() : Boolean
    {
        var count = 0


            if(etEmail!!.text.isEmpty())
            {
                tilEmail!!.error = "Provide your email"
            }
            else
            {
                tilEmail!!.error = null
                count++
            }

            if(etPass!!.text.isEmpty())
            {
                tilPass!!.error = "Enter your password"
            }
            else
            {
                tilPass!!.error = null
                count++
            }

        if(count==2)
            return true
        else
            return false
    }

    override fun onStart() {
        super.onStart()

        currentUser = mAuth!!.currentUser
    }

    fun signUp()
    {
        mAuth!!.createUserWithEmailAndPassword(etEmail!!.text.toString(), etPass!!.text.toString())
                .addOnCompleteListener { task: Task<AuthResult> ->
                    if (task.isSuccessful)
                    {
                        user = mAuth!!.currentUser

                        getSharedPreferences("user", Context.MODE_PRIVATE).edit().putString("email", user!!.email).putString("id", user!!.uid).putString("pass", etPass!!.text.toString()).commit()
                        startActivity(Intent(this, CollapsingToolbarActivity::class.java))
                    }
                    else
                    {
                        Toast.makeText(this, "Sign Up Failed", Toast.LENGTH_SHORT)
                    }
                    hideLoader()
                }
    }


    fun login()
    {
        mAuth!!.signInWithEmailAndPassword(etEmail!!.text.toString(), etPass!!.text.toString())
                .addOnCompleteListener { task ->
                    if(task.isSuccessful)
                    {
                        user = mAuth!!.currentUser
                        getSharedPreferences("user", Context.MODE_PRIVATE).edit().putString("email", user!!.email).putString("id", user!!.uid).putString("pass", etPass!!.text.toString()).commit()
                        startActivity(Intent(this, CollapsingToolbarActivity::class.java))
                    }
                    else
                    {
                        Toast.makeText(this, "Login Failed", Toast.LENGTH_SHORT)
                    }

                    hideLoader()
                }
    }


    fun hideKeyboard(activity: Activity) {
        val view = activity.currentFocus
        if (view != null) {
            val imm = activity.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.hideSoftInputFromWindow(view.windowToken, 0)
        }
    }
}

