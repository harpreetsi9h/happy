package com.example.android.happy

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.*
import android.widget.TextView
import android.widget.LinearLayout
import java.util.*
import kotlin.collections.ArrayList
import android.support.v4.view.MenuItemCompat
import android.support.v7.widget.SearchView
import android.view.MenuInflater



class RecyclerSearchActivity : AppCompatActivity(), SearchView.OnQueryTextListener {

    var countriesList : MutableList<CountryModel> = ArrayList()
    var recyclerView : RecyclerView? = null
    var rvAdapter : RVAdapter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_recycler_search)

        recyclerView = findViewById(R.id.recyclerSearch) as RecyclerView


        var locales : Array<String> = Locale.getISOCountries()

        for(isoCode in locales)
        {
            val obj : Locale = Locale("", isoCode)
            val countryModel : CountryModel = CountryModel(obj.displayCountry, obj.isO3Country)
            countriesList.add(countryModel)
        }


        val layoutManager : LinearLayoutManager = LinearLayoutManager(this, LinearLayout.VERTICAL, false)

        rvAdapter = RVAdapter(countriesList)

        recyclerView!!.layoutManager = layoutManager
        recyclerView!!.adapter = rvAdapter

    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {

        var inflater = MenuInflater(this);
        var menu = inflater.inflate(R.menu.recycler_search, menu) as Menu;

        val menuSearch = findViewById(R.id.menu_recycler_search) as MenuItem
        val searchView  = MenuItemCompat.getActionView(menuSearch) as SearchView

        searchView.setOnQueryTextListener(this)

        MenuItemCompat.setOnActionExpandListener(menuSearch,
                object : MenuItemCompat.OnActionExpandListener {
                    override fun onMenuItemActionCollapse(item: MenuItem): Boolean {
                        // Do something when collapsed
                        rvAdapter!!.setFilter(countriesList)
                        return true // Return true to collapse action view
                    }

                    override fun onMenuItemActionExpand(item: MenuItem): Boolean {
                        // Do something when expanded
                        return true // Return true to expand action view
                    }
                })

        return super.onCreateOptionsMenu(menu)
    }


    override fun onQueryTextSubmit(query: String?): Boolean {
        return false
    }

    override fun onQueryTextChange(newText: String): Boolean {
        val filteredModelList = filter(countriesList, newText)

        rvAdapter!!.setFilter(filteredModelList)
        return true
    }


    private fun filter(models: List<CountryModel>, query: String): List<CountryModel> {
        var query = query
        query = query.toLowerCase()
        var filteredModelList : MutableList<CountryModel> = ArrayList()
        for (model in models) {
            val text = model.name.toLowerCase()
            if (text.contains(query)) {
                filteredModelList.add(model)
            }
        }
        return filteredModelList
    }

    class RVAdapter(countriesList : MutableList<CountryModel>) : RecyclerView.Adapter<ItemViewHolder>()
    {
        var countriesList : MutableList<CountryModel> = ArrayList()
        init {
            this.countriesList = countriesList
        }

        override fun onBindViewHolder(holder: ItemViewHolder?, position: Int) {
            holder!!.countryName.setText(countriesList.get(position).name)
            holder.countryIsoCode.setText(countriesList.get(position).isoCode)
        }

        override fun getItemCount(): Int {
            return countriesList.size
        }

        override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): ItemViewHolder {
            val view = LayoutInflater.from(parent!!.getContext()).inflate(R.layout.item_recycler_search, parent, false)

            return ItemViewHolder(view)
        }

        fun setFilter(countryModels: List<CountryModel>) {
            countriesList.addAll(countryModels)
            notifyDataSetChanged()
        }

    }

    class ItemViewHolder(itemView: View?) : RecyclerView.ViewHolder(itemView) {
        public var countryName = itemView!!.findViewById(R.id.country_name) as TextView
        public var countryIsoCode = itemView!!.findViewById(R.id.country_name) as TextView
    }

    data class CountryModel(internal var name: String, internal var isoCode: String)
}
