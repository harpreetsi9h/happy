package com.example.android.happy.Activity

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.util.Log
import android.view.View
import android.widget.FrameLayout
import android.widget.ImageButton
import butterknife.BindView
import butterknife.ButterKnife
import butterknife.OnClick
import com.example.android.happy.HelperClass.MyAnimationListener
import com.example.android.happy.R
import kotlinx.android.synthetic.main.activity_collapsing.view.*
import kotlinx.android.synthetic.main.layout_news_item.*

class AnimatoinDemoActivity : AppCompatActivity() {

    @BindView(R.id.imgAdd)
    lateinit var imgAdd : ImageButton

    @BindView(R.id.ibCamera)
    lateinit var ibCamera : ImageButton

    @BindView(R.id.ibPhoto)
    lateinit var ibPhoto : ImageButton

    @BindView(R.id.ibStorage)
    lateinit var ibStorage : ImageButton

    @BindView(R.id.ibVideo)
    lateinit var ibVideo : ImageButton

    @BindView(R.id.rootLayout)
    lateinit var rootLayout : FrameLayout

    var show = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_animatoin_demo)
        ButterKnife.bind(this)
        supportActionBar!!.title = "Animation Demo"
    }

    @OnClick(R.id.imgAdd, R.id.ibCamera, R.id.ibPhoto, R.id.ibStorage, R.id.ibVideo)
    fun onClick(view: View)
    {
        when(view.id)
        {
            R.id.imgAdd ->
            {
                if(show)
                {
                    imgAdd.animate().rotationBy(-45f).start()
                    animateCollapse()
                    show = false
                }
                else
                {
                    imgAdd.animate().rotationBy(45f).start()
                    animateUp()
                    show = true
                }
            }
            R.id.ibCamera -> Snackbar.make(rootLayout, "Camera Clicked", Snackbar.LENGTH_SHORT).show()
            R.id.ibPhoto -> Snackbar.make(rootLayout, "Photo Clicked", Snackbar.LENGTH_SHORT).show()
            R.id.ibStorage -> Snackbar.make(rootLayout, "Storage Clicked", Snackbar.LENGTH_SHORT).show()
            R.id.ibVideo -> Snackbar.make(rootLayout, "Video Clicked", Snackbar.LENGTH_SHORT).show()
        }
    }

    fun animateUp()
    {

        ibCamera.animate().translationX(-330f).translationY(-350f)
                .setListener(MyAnimationListener(ibCamera, false)).start()
        ibPhoto.animate().translationX(-120f).translationY(-400f)
                .setListener(MyAnimationListener(ibPhoto, false)).start()
        ibStorage.animate().translationX(+ 120f).translationY(-400f)
                .setListener(MyAnimationListener(ibStorage, false)).start()
        ibVideo.animate().translationX(+ 330f).translationY(- 350f)
                .setListener(MyAnimationListener(ibVideo, false)).start()
    }

    fun animateCollapse()
    {
        ibCamera.animate().translationX(0f).translationY(0f)
                .setListener(MyAnimationListener(ibCamera, true)).start()
        ibPhoto.animate().translationX(0f).translationY(0f)
                .setListener(MyAnimationListener(ibPhoto, true)).start()
        ibStorage.animate().translationX(0f).translationY(0f)
                .setListener(MyAnimationListener(ibStorage, true)).start()
        ibVideo.animate().translationX(0f).translationY(0f)
                .setListener(MyAnimationListener(ibVideo, true)).start()

    }
}
