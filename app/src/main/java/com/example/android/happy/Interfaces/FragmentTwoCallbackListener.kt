package com.example.android.happy.Interfaces

import android.os.Bundle

/**
 * Created by android on 9/23/17.
 */
interface FragmentTwoCallbackListener
{
    public fun fragmentTwoCallback(bundle: Bundle)
}