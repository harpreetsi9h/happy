package com.example.android.happy.Activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import com.example.android.happy.R;

/**
 * Created by android on 7/5/17.
 */

public class BaseActivity extends AppCompatActivity
{
    Toolbar toolbar;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        toolbar = (Toolbar) findViewById(R.id.toolbar);

    }

    void setToolbarTitle(String title)
    {
        toolbar.setTitle(title);
    }
}
