package com.example.android.happy.Adapter;

import android.app.Activity;
import android.database.Cursor;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.example.android.happy.R;
import com.facebook.drawee.view.SimpleDraweeView;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by android on 7/22/17.
 */

public class GalleryAdapter extends RecyclerView.Adapter<GalleryAdapter.GalleryVH>{

    List<String> imgList;

    public GalleryAdapter(List<String> imgList) {
        this.imgList = imgList;
    }

    @Override
    public GalleryVH onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_gallery, parent, false);
        return new GalleryVH(itemView);
    }

    @Override
    public void onBindViewHolder(GalleryVH holder, int position) {
        Uri uri = Uri.parse(imgList.get(position));
        holder.sdv.setImageURI(uri);
    }

    @Override
    public int getItemCount() {
        return imgList.size();
    }

    class GalleryVH extends RecyclerView.ViewHolder
    {
        SimpleDraweeView sdv;
        public GalleryVH(View itemView) {
            super(itemView);

            sdv = (SimpleDraweeView) itemView.findViewById(R.id.sdv);
        }
    }


}
