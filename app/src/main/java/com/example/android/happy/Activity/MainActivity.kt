package com.example.android.happy

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v4.app.ActivityCompat
import android.support.v4.app.ActivityOptionsCompat
import android.support.v7.widget.AppCompatButton
import android.util.Log
import android.view.View
import android.widget.Button
import butterknife.BindView
import butterknife.ButterKnife
import butterknife.OnClick
import com.example.android.happy.Activity.*

class MainActivity : AppCompatActivity(), View.OnClickListener {

    @BindView(R.id.btnCollapsing)
    lateinit var btnCollapsing : Button

    @BindView(R.id.btnRecyclerSearch)
    lateinit var btnRecyclerSearch : Button

    @BindView(R.id.btnVideoRecorder)
    lateinit var btnVideoRecorder : Button

    @BindView(R.id.btnCustomUi)
    lateinit var btnCustomUi : Button

    @BindView(R.id.btnOfflineMap)
    lateinit var btnOfflineMap : Button

    @BindView(R.id.btnTest)
    lateinit var btnTest : Button

    @BindView(R.id.btnZoomAnim)
    lateinit var btnZoomAnim : Button

    @BindView(R.id.btnGallery)
    lateinit var btnGallery : Button

    @BindView(R.id.btnFragDemo)
    lateinit var btnFragDemo : Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        ButterKnife.bind(this)


//        val unsorted = intArrayOf(66, 34, 98, 55, 46, 12, 23)
//
//        for (i in 0..unsorted.size)
//        {
//            Log.d("sortIssue", "i: $i")
//            for(j in 1..(unsorted.size-i))
//            {
//                Log.d("sortIssue", "j: $j")
//                if(unsorted[j-1]>unsorted[j])
//                {
//                    val temp = unsorted[j-1]
//                    unsorted[j-1] = unsorted[j]
//                    unsorted[j] = temp
//                }
//            }
//        }
//
//        println("Sorted Arrays: ")
//
//        for(i in 0..unsorted.size)
//        {
//            print("${unsorted[i]}, ")
//        }
    }

    @OnClick(R.id.btnCollapsing, R.id.btnRecyclerSearch, R.id.btnVideoRecorder, R.id.btnCustomUi,
            R.id.btnOfflineMap, R.id.btnTest, R.id.btnZoomAnim, R.id.btnGallery, R.id.btnFragDemo,
            R.id.btnCutDemo, R.id.btnAnimDemo)
    override fun onClick(v: View) {

        val transitionName = resources.getString(R.string.transition_start_activity)

        var options : ActivityOptionsCompat? = null;
        var intent : Intent? = null;
        when(v.id)
        {
            R.id.btnCollapsing ->
            {
                options = ActivityOptionsCompat.makeSceneTransitionAnimation(this, btnCollapsing, transitionName)
                intent = Intent(this, CollapsingToolbarActivity::class.java)
            }
            R.id.btnRecyclerSearch ->
            {
                options = ActivityOptionsCompat.makeSceneTransitionAnimation(this, btnRecyclerSearch, transitionName)
                intent = Intent(this, RecyclerSearchActivity::class.java)
            }
            R.id.btnVideoRecorder -> {
                options = ActivityOptionsCompat.makeSceneTransitionAnimation(this, btnVideoRecorder, transitionName)
                intent = Intent(this, VideoRecorderActivity::class.java)
            }
            R.id.btnCustomUi -> {
                options = ActivityOptionsCompat.makeSceneTransitionAnimation(this, btnCustomUi, transitionName)
                intent = Intent(this, CustomUi::class.java)
            }
            R.id.btnOfflineMap -> {
                options = ActivityOptionsCompat.makeSceneTransitionAnimation(this, btnOfflineMap, transitionName)
                intent = Intent(this, OfflineMapsActivity::class.java)
            }
            R.id.btnTest -> {
                options = ActivityOptionsCompat.makeSceneTransitionAnimation(this, btnTest, transitionName)
                intent = Intent(this, TestActivity::class.java)
            }
            R.id.btnZoomAnim -> {
                options = ActivityOptionsCompat.makeSceneTransitionAnimation(this, btnZoomAnim, transitionName)
                intent = Intent(this, ZoomAnimActivity::class.java)
            }
            R.id.btnGallery -> {
                options = ActivityOptionsCompat.makeSceneTransitionAnimation(this, btnGallery, transitionName)
                intent = Intent(this, GalleryActivity::class.java)
            }
            R.id.btnFragDemo ->
            {
                options = ActivityOptionsCompat.makeSceneTransitionAnimation(this, btnFragDemo, transitionName)
                intent = Intent(this, FragmentsDemoActivity::class.java)
            }
            R.id.btnCutDemo -> intent = Intent(this, CutLayoutDemoActivity::class.java)
            R.id.btnAnimDemo -> intent = Intent(this, AnimatoinDemoActivity::class.java)
        }

        startActivity(intent!!)
    }
}
