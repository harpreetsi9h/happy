package com.example.android.happy.Activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Spinner;

import com.example.android.happy.Adapter.SpinnerAdapter;
import com.example.android.happy.R;

import java.util.ArrayList;

public class CustomUi extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_custom_ui);

        Spinner sp = (Spinner) findViewById(R.id.spinner);

        ArrayList spList = new ArrayList();
        spList.add("One");
        spList.add("Two");
        spList.add("Three");
        spList.add("Four");
        spList.add("Five");
        spList.add("Six");
        spList.add("Seven");
        spList.add("Eight");

        SpinnerAdapter spAdapter = new SpinnerAdapter(this, android.R.layout.simple_spinner_item, spList);

        sp.setAdapter(spAdapter);

    }
}
