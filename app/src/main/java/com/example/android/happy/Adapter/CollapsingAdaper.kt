package com.example.android.happy.Adapter

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.android.happy.R

/**
 * Created by developtech on 11/9/17.
 */
class CollapsingAdaper : RecyclerView.Adapter<CollapsingVH>(){
    override fun onBindViewHolder(holder: CollapsingVH?, position: Int) {
    }

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): CollapsingVH {
        val itemView = LayoutInflater.from(parent!!.context).inflate(R.layout.item_collapsing, parent, false)
        return CollapsingVH(itemView)
    }

    override fun getItemCount(): Int {
        return 5
    }
}

class CollapsingVH(itemView: View) : RecyclerView.ViewHolder(itemView)
{

}