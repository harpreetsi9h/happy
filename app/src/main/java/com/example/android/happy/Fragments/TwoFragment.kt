package com.example.android.happy.Fragments


import android.annotation.SuppressLint
import android.app.Activity
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import butterknife.BindView
import butterknife.ButterKnife
import butterknife.OnClick
import com.example.android.happy.Activity.FragmentsDemoActivity
import com.example.android.happy.Interfaces.FragmentTwoCallbackListener

import com.example.android.happy.R


class TwoFragment : Fragment() {

    @BindView(R.id.etName)
    lateinit var etName : EditText

    @BindView(R.id.etPhone)
    lateinit var etPhone : EditText

    var activity : FragmentsDemoActivity? = null

    override fun onAttach(activity: Activity?) {
        super.onAttach(activity)

        this.activity = activity as FragmentsDemoActivity
    }

    @SuppressLint("NewApi")
    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view : View = inflater!!.inflate(R.layout.fragment_two, container, false)
        ButterKnife.bind(this, view)
        activity!!.toolbar.title = "Two"
        return view
    }

    @OnClick(R.id.btnSubmit)
    public fun onClick(view : View)
    {
        when(view.id)
        {
            R.id.btnSubmit->
            {
                val bundle = Bundle()
                bundle.putString("name", etName.text.toString())
                bundle.putString("phone", etPhone.text.toString())

                activity!!.fragmentTwoCallback(bundle)
            }
        }
    }
}
