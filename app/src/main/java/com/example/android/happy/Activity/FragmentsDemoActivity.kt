package com.example.android.happy.Activity

import android.annotation.SuppressLint
import android.graphics.Color
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.LinearLayout
import butterknife.BindView
import butterknife.ButterKnife
import com.example.android.happy.R
import android.os.Build
import android.support.v4.app.Fragment
import android.view.View
import com.example.android.happy.R.id.rootLayout
import android.view.View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR
import android.widget.FrameLayout
import android.widget.Toolbar
import com.example.android.happy.Fragments.OneFragment
import com.example.android.happy.Interfaces.FragmentTwoCallbackListener


class FragmentsDemoActivity : AppCompatActivity(){

    @BindView(R.id.rootLayout)
    lateinit var rootLayout : LinearLayout

    @BindView(R.id.contentLayout)
    lateinit var content : FrameLayout

    @BindView(R.id.toolbar)
    public lateinit var toolbar : android.support.v7.widget.Toolbar

    public fun fragmentTwoCallback(bundle: Bundle) {
        val fragment : Fragment = OneFragment()
        fragment.arguments = bundle
        supportFragmentManager.beginTransaction().replace(R.id.contentLayout,fragment).commit()
    }

    @SuppressLint("NewApi")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_fragments_demo)
        ButterKnife.bind(this)

       toolbar.title = "Fragments Demo"

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {

            var flags = rootLayout.systemUiVisibility
            flags = flags or View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR
            rootLayout.systemUiVisibility = flags
            window.statusBarColor = Color.WHITE
        } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP && Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            window.statusBarColor = resources.getColor(R.color.colorLightGrey)
        }

        val frag : Fragment = OneFragment()
        supportFragmentManager.beginTransaction().replace(R.id.contentLayout, frag).commit()
    }
}
