package com.example.android.happy.Activity;

import android.app.Activity;
import android.content.Context;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.hardware.Camera;
import android.media.CamcorderProfile;
import android.media.MediaRecorder;
import android.net.Uri;
import android.os.Environment;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Surface;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;


import com.example.android.happy.R;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

import static android.provider.MediaStore.Files.FileColumns.MEDIA_TYPE_VIDEO;

public class VideoRecorderActivity extends AppCompatActivity implements  View.OnClickListener, MediaRecorder.OnInfoListener, SurfaceHolder.Callback
{


    Camera camera;
    Button record;
    MediaRecorder mediaRecorder;
    SurfaceView videoView;
    SurfaceHolder surfaceHolder;

    ProgressBar pbRecording;

    Timer timer;

    boolean combineFiles;

    public static int orientation;

    String TAG = "recordingissue";

    private boolean isRecording;

    private static final int VIDEO_LENGTH = 30000;

    ImageView ivFlip;

    ArrayList videosPathList;

    int currentCamera = Camera.CameraInfo.CAMERA_FACING_BACK;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video_recorder);

        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        videoView = (SurfaceView) findViewById(R.id.videoView);
        record = (Button) findViewById(R.id.btnRecord);
        pbRecording = (ProgressBar) findViewById(R.id.pbRecording);

        ivFlip = (ImageView) findViewById(R.id.ivFlip);

        ivFlip.setOnClickListener(this);
        record.setOnClickListener(this);

        if(!checkCameraHardware(this))
            return;

        camera = getCameraInstance();

        if(camera==null)
        {
            Toast.makeText(this, "Camera is null", Toast.LENGTH_SHORT).show();
        }

//        myCameraSurfaceView = new MyCameraSurfaceView(this,this, camera);



        surfaceHolder = videoView.getHolder();

        surfaceHolder.addCallback(this);
        // deprecated setting, but required on Android versions prior to 3.0
        surfaceHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);

        setCameraDisplayOrientation(this, currentCamera, camera);

        try {
            if(camera!=null){
                camera.setPreviewDisplay(surfaceHolder);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        camera.startPreview();

        changeButton();

        pbRecording.setMax(VIDEO_LENGTH);

        videosPathList = new ArrayList();

    }

    void startProgress()
    {
        timer = new Timer();

        timer.schedule(new TimerTask() {

            @Override
            public void run() {

                if(isRecording)  // call ui only when  the progress is not stopped
                {
                    runOnUiThread(new Runnable() {

                        @Override
                        public void run() {
                            try
                            {

                                pbRecording.setProgress(pbRecording.getProgress()+50);

                            } catch (Exception e) {}

                        }
                    });
                }
            }



        }, 1, 50);
    }


    private Camera getCameraInstance(){
        // TODO Auto-generated method stub
        Camera c = null;
        try {
            c = Camera.open(Camera.CameraInfo.CAMERA_FACING_BACK); // attempt to get a Camera instance
        }
        catch (Exception e){
            // Camera is not available (in use or does not exist)
        }
        return c; // returns null if camera is unavailable
    }

    void changeButton()
    {
        if(isRecording)
        {
            record.setText("Stop");
            record.setBackgroundColor(getResources().getColor(android.R.color.holo_red_light));

            startProgress();
        }
        else
        {
            record.setText("Start");
            record.setBackgroundColor(getResources().getColor(android.R.color.holo_green_light));

            if(timer!=null)
            {
                timer.cancel();
                timer=null;
            }
            pbRecording.setProgress(0);
        }
    }

    public void flipCamera() {
        releaseMediaRecorder();
        //myCamera is the Camera object
        if (Camera.getNumberOfCameras()>=2) {

            if(isRecording)
            {

            }
            camera.stopPreview();
            camera.release();

            //"which" is just an integer flag
            if(currentCamera == Camera.CameraInfo.CAMERA_FACING_BACK)
            {
                currentCamera = Camera.CameraInfo.CAMERA_FACING_FRONT;
            }
            else
            {
                currentCamera = Camera.CameraInfo.CAMERA_FACING_BACK;
            }
            try {
                //"this" is a SurfaceView which implements SurfaceHolder.Callback,
                //as found in the code examples
                camera = Camera.open(currentCamera);

                setCameraDisplayOrientation(this, currentCamera, camera);

                try {
                    camera.setPreviewDisplay(surfaceHolder);
                } catch (IOException e) {
                    e.printStackTrace();
                }

                camera.startPreview();

            } catch (Exception exception) {
                camera.release();
                camera = null;
            }
        }
    }

    private boolean prepareVideoRecorder(){

        if(camera!=null)
            setCameraDisplayOrientation(this, 0, camera);

        mediaRecorder = new MediaRecorder();
        mediaRecorder.setOnInfoListener(this);
        // Step 1: Unlock and set camera to MediaRecorder
        camera.unlock();
        mediaRecorder.setCamera(camera);

        // Step 2: Set sources
        mediaRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
        mediaRecorder.setVideoSource(MediaRecorder.VideoSource.DEFAULT);

        // Step 3: Set a CamcorderProfile (requires API Level 8 or higher)
        mediaRecorder.setProfile(CamcorderProfile.get(CamcorderProfile.QUALITY_480P));

        // Step 4: Set output file
        File mediaFile = getOutputMediaFile(MEDIA_TYPE_VIDEO);
        videosPathList.add(mediaFile.getAbsolutePath());
        mediaRecorder.setOutputFile(getOutputMediaFile(MEDIA_TYPE_VIDEO).toString());


        mediaRecorder.setMaxDuration(VIDEO_LENGTH);

        // Step 5: Set the preview output
        mediaRecorder.setPreviewDisplay(surfaceHolder.getSurface());
        // Step 6: Prepare configured MediaRecorder

        if(currentCamera == Camera.CameraInfo.CAMERA_FACING_BACK)
        {
            mediaRecorder.setOrientationHint(90);
        }
        else if(currentCamera == Camera.CameraInfo.CAMERA_FACING_FRONT)
        {
            mediaRecorder.setOrientationHint(270);
        }

        try {
            mediaRecorder.prepare();
        } catch (IllegalStateException e) {
            Log.d(TAG, "IllegalStateException preparing MediaRecorder: " + e.getMessage());
            releaseMediaRecorder();
            return false;
        } catch (IOException e) {
            Log.d(TAG, "IOException preparing MediaRecorder: " + e.getMessage());
            releaseMediaRecorder();
            return false;
        }

        Log.d(TAG, "prepareVideoRecorder: return true ");
        return true;
    }

    /** Check if this device has a camera */
    private boolean checkCameraHardware(Context context) {
        if (context.getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA)){
            // this device has a camera
            return true;
        } else {
            // no camera on this device
            return false;
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        releaseMediaRecorder();       // if you are using MediaRecorder, release it first
        releaseCamera();              // release the camera immediately on pause event
    }

    private void releaseMediaRecorder(){
        if (mediaRecorder != null) {
            mediaRecorder.reset();   // clear recorder configuration
            mediaRecorder.release(); // release the recorder object
            mediaRecorder = null;
            camera.lock();           // lock camera for later use
        }
    }

    private void releaseCamera(){
        if (camera != null){
            camera.release();        // release the camera for other applications
            camera = null;
        }
    }



    public static final int MEDIA_TYPE_IMAGE = 1;
    public static final int MEDIA_TYPE_VIDEO = 2;

    /** Create a file Uri for saving an image or video */
    private static Uri getOutputMediaFileUri(int type){
        return Uri.fromFile(getOutputMediaFile(type));
    }

    /** Create a File for saving an image or video */
    private static File getOutputMediaFile(int type){
        // To be safe, you should check that the SDCard is mounted
        // using Environment.getExternalStorageState() before doing this.

        File mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_DOWNLOADS), "MyVideoRecorderApp");
        // This location works best if you want the created images to be shared
        // between applications and persist after your app has been uninstalled.

        // Create the storage directory if it does not exist
        if (! mediaStorageDir.exists()){
            if (! mediaStorageDir.mkdirs()){
                Log.d("MyVideoRecorderApp", "failed to create directory");
                return null;
            }
        }

        // Create a media file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        File mediaFile;
        if (type == MEDIA_TYPE_IMAGE){
            mediaFile = new File(mediaStorageDir.getPath() + File.separator +
                    "IMG_"+ timeStamp + ".jpg");
        } else if(type == MEDIA_TYPE_VIDEO) {
            mediaFile = new File(mediaStorageDir.getPath() + File.separator +
                    "VID_"+ timeStamp + ".mp4");
        } else {
            return null;
        }

        return mediaFile;
    }

    void stopRecording()
    {

        releaseMediaRecorder(); // release the MediaRecorder object
        camera.lock();         // take camera access back from MediaRecorder

        // inform the user that recording has stopped
        isRecording = false;
        changeButton();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.btnRecord:
            {
                if (isRecording) {
                    // stop recording and release camera
                   stopRecording();

                   combineFiles();
                } else {
                    // initialize video camera
                    if (prepareVideoRecorder()) {
                        // Camera is available and unlocked, MediaRecorder is prepared,
                        // now you can start recording
                        mediaRecorder.start();

                        // inform the user that recording has started
                        isRecording = true;
                        changeButton();
                    }
                }


                break;
            }
            case R.id.ivFlip:
            {
                if(isRecording)
                {

                    releaseMediaRecorder(); // release the MediaRecorder object
                    camera.lock();         // take camera access back from MediaRecorder

                    flipCamera();
                    if (prepareVideoRecorder()) {
                        // Camera is available and unlocked, MediaRecorder is prepared,
                        // now you can start recording
                        mediaRecorder.start();
                    }

                    combineFiles = true;
                }
                else
                {
                    flipCamera();
                    combineFiles = false;
                }

            }
        }
    }

    private void combineFiles() {

    }

    @Override
    public void onInfo(MediaRecorder mr, int what, int extra) {
        if(what==MediaRecorder.MEDIA_RECORDER_INFO_MAX_DURATION_REACHED)
        {
            stopRecording();
        }
    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {
        if(holder!=null) {
            try {
                camera.setPreviewDisplay(holder);
                camera.startPreview();
            } catch (IOException e) {
            }
        }
    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {

    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {

    }


    public class MyCameraSurfaceView extends SurfaceView implements SurfaceHolder.Callback{

        private SurfaceHolder mHolder;
        private Activity mActivity;
        private Camera camera;

        public MyCameraSurfaceView(Context context,Activity activity, Camera camera) {
            super(context);
            mActivity=activity;
            this.camera = camera;
            // Install a SurfaceHolder.Callback so we get notified when the
            // underlying surface is created and destroyed.
            mHolder = getHolder();
            mHolder.addCallback(this);
            // deprecated setting, but required on Android versions prior to 3.0
            mHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
        }

        public void surfaceChanged(SurfaceHolder holder, int format, int width, int height)
        {
            try {
                setCameraDisplayOrientation(mActivity,0,camera);
                previewCamera();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        public void previewCamera()
        {
            try
            {
                camera.setPreviewDisplay(mHolder);
                camera.startPreview();
            }
            catch(Exception e)
            {
                //Log.d(APP_CLASS, "Cannot start preview", e);
            }
        }


        @Override
        public void surfaceCreated(SurfaceHolder holder) {
            if(holder!=null) {
                try {
                    camera.setPreviewDisplay(holder);
                    camera.startPreview();
                } catch (IOException e) {
                }
            }
        }

        @Override
        public void surfaceDestroyed(SurfaceHolder holder) {
            // TODO Auto-generated method stub

        }


    }


    public static void setCameraDisplayOrientation(Activity activity,
                                                   int cameraId, android.hardware.Camera camera) {

        android.hardware.Camera.CameraInfo info =
                new android.hardware.Camera.CameraInfo();

        android.hardware.Camera.getCameraInfo(cameraId, info);

        int rotation = activity.getWindowManager().getDefaultDisplay().getRotation();
        int degrees = 0;

        switch (rotation) {
            case Surface.ROTATION_0: degrees = 0; break;
            case Surface.ROTATION_90: degrees = 90; break;
            case Surface.ROTATION_180: degrees = 180; break;
            case Surface.ROTATION_270: degrees = 270; break;
        }

        int result;
        if (cameraId == Camera.CameraInfo.CAMERA_FACING_FRONT) {
            result = (info.orientation + degrees) % 360;
            result = (360 - result) % 360;  // compensate the mirror

            Log.d("orientationIssue", "setCameraDisplayOrientation info.orientatin: "+info.orientation+", degrees: "+degrees);
        } else {  // back-facing
            result = (info.orientation - degrees + 360) % 360;

        }
        VideoRecorderActivity.orientation=result;
        if(camera!=null) {
            camera.setDisplayOrientation(result);
            camera.getParameters().setFocusMode(Camera.Parameters.FOCUS_MODE_AUTO);
        }
    }

}
