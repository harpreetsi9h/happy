package com.example.android.happy.HelperClass;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

/**
 * Created by android on 7/1/17.
 */

public class Utils
{
    public static boolean isInternetOn(Context paramContext)
    {
        NetworkInfo localNetworkInfo = ((ConnectivityManager) paramContext.getSystemService(Context.CONNECTIVITY_SERVICE)).getActiveNetworkInfo();
        if (localNetworkInfo != null)
        {
            return localNetworkInfo.isConnected();
        }
        return false;
    }
}
