package com.example.android.happy.Network;

import com.example.android.happy.Model.NewsResponse;


import retrofit2.http.GET;
import rx.Observable;

/**
 * Created by android on 7/6/17.
 */

public interface NetworkApi {

    @GET("v1/articles?source=techcrunch&apiKey=4098019a998a47a5b72765c61a9b689c")
    Observable<NewsResponse> getNews();
}
