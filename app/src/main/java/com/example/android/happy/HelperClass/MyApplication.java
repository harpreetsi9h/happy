package com.example.android.happy.HelperClass;

import android.app.Application;

import com.facebook.drawee.backends.pipeline.Fresco;
import com.mapbox.mapboxsdk.Mapbox;

/**
 * Created by android on 7/1/17.
 */

public class MyApplication extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        Fresco.initialize(this);
        // Mapbox Access token
        Mapbox.getInstance(this, "pk.eyJ1IjoiaGFycHJlZXRzaTloIiwiYSI6ImNqNGw1YzM4bTBxNGYyd3FqdXJyNTNkODUifQ.4WvYjLfbFtzfxsPVyyBT-g");
    }
}
